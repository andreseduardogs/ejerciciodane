/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ufps.Negocio;

import java.time.LocalDate;
import ufps.Modelo.Region;
import ufps.util.colecciones_seed.ListaCD;
import ufps.util.varios.ArchivoLeerTexto;
import ufps.Modelo.*;
import ufps.util.colecciones_seed.Pila;
import ufps.util.varios.ArchivoLeerURL;

/**
 *
 * @author MADARME
 */
public class Dane {
    
    private ListaCD<Region> regiones=new ListaCD();

    public Dane() {
    }

    public Dane(String url)
    {
    this.leerUrLDane(url);
    }
    
    
    public ListaCD<Region> getRegiones() {
        return regiones;
    }

    public void setRegiones(ListaCD<Region> regiones) {
        this.regiones = regiones;
    }
    
    
   
    /*
    
REGION,CÓDIGO DANE DEL DEPARTAMENTO,DEPARTAMENTO,CÓDIGO DANE DEL MUNICIPIO,MUNICIPIO
Región Eje Cafetero - Antioquia,5,Antioquia,5001,Medellín
    
    */
    
    
    private void leerUrLDane(String url)
    {

        ArchivoLeerURL file=new ArchivoLeerURL(url);
        Object v[]=file.leerArchivo();
        int codigoRegion=1;
        for(int i=1;i<v.length;i++)
        {
        
        String registro=v[i].toString();
        //Región Eje Cafetero , 5,Antioquia,5001,Medellín
        String datoRegion[]=registro.split(",");
        /*
            datoRegion[0]=Región Eje Cafetero
            datoRegion[1]=5
            datoRegion[2]= Antioquia
            datoRegion[3]=5001
            datoRegion[4]=Medellín
            
        */
        
        Region nuevaRegion=new Region(codigoRegion,datoRegion[0]);
        Region buscar=this.buscarRegion(nuevaRegion);
        if(buscar==null)
        {
            this.regiones.insertarAlFinal(nuevaRegion);
            codigoRegion++;
            buscar=nuevaRegion;
        }
        this.crearDepartamento(buscar, datoRegion);
        
        
    }
    }
    
    
    private void crearDepartamento(Region r, String datoRegion[])
    {
    /*
            datoRegion[0]=Región Eje Cafetero
            datoRegion[1]=5
            datoRegion[2]= Antioquia
            datoRegion[3]=5001
            datoRegion[4]=Medellín
            
        */
        
        Departamento nuevo=new Departamento(Integer.parseInt(datoRegion[1]),datoRegion[2]);
        Departamento buscado=r.buscarDpto(nuevo);
        if(buscado==null)
        {
            r.getDptos().insertarAlFinal(nuevo);
            buscado=nuevo;
        }
        
        this.crearMunicipio(buscado, datoRegion);
        
    
    }
    
    
    
    
    private void crearMunicipio(Departamento d, String datoRegion[])
    {
    
        /*
            datoRegion[0]=Región Eje Cafetero
            datoRegion[1]=5
            datoRegion[2]= Antioquia
            datoRegion[3]=5001
            datoRegion[4]=Medellín
            
        */
    Municipio nuevo=new Municipio(Integer.parseInt(datoRegion[3]),datoRegion[4]);
    d.getMunicipios().insertarAlFinal(nuevo);
    }
    
    private Region buscarRegion(Region nueva)
    {
    if(this.regiones.esVacia())
        return null;
    for(Region r:this.regiones)
    {
        if(r.equals(nueva))
            return r;
    }
    return null;
    }

    @Override
    public String toString() {
        String msg="";
        for(Region r:this.regiones)
        {
            msg+="\n***********************************************************************\n";
            msg+="Region:"+r.getNombre()+": Código de región:"+r.getCodigo()+"\n";
            for(Departamento d:r.getDptos())
            {
                msg+="Departamento:"+d.getNombre()+" Código dpto:"+d.getCodigo()+"\n";
                
                for(Municipio m:d.getMunicipios())
                {
                    msg+="Municipio:"+m.getNombre()+"\n";
                    
                    //Cuando este proceso se realice , se borra la cola de personas
                    //::::::: ADVERTENCIA  ::::::::::::
                    while(!m.getPersonas().esVacia())
                    {
                    msg+="\n Persona:"+m.getPersonas().deColar().toString();
                    }
                }
                
            }
        }
        return msg;
    }
    
    
    
    public String getListadoRegiones()
    {
    String msg="";
    for(Region r:this.regiones)
        {
            
            msg+="Region:"+r.getNombre()+": Código de región:"+r.getCodigo()+" , Cantidad de subsidios:"+r.getCantidadBeneficiarios()+"\n";
    
        }
    return msg;
    
    }
    
    
    
    //Punto 2. 
    public void cargarSubsidioRegion(String url)
    {
       
        ArchivoLeerURL file=new ArchivoLeerURL(url);
        Object v[]=file.leerArchivo();
        
        for(int i=1;i<v.length;i++)
        {
        
        String registro=v[i].toString();
        /**
         * codigo_region;cantidad de subsidios
            1;3
            2;3
            3;2
            4;3
            5;3
            6;4
         */
        String datoRegion[]=registro.split(";");
        int codRegion=Integer.parseInt(datoRegion[0]);
        int canSub=Integer.parseInt(datoRegion[1]);
        //Suponemos que el archivo NO CONTIENE DATOS ERRADOS
        this.regiones.get(codRegion-1).setCantidadBeneficiarios(canSub);
        
        
        }
    }
    
    //Punto 3.
    public void cargarPersonas(String url)
    {
        ArchivoLeerURL file=new ArchivoLeerURL(url);
        Object v[]=file.leerArchivo();
        
        for(int i=1;i<v.length;i++)
        {
            String registro=v[i].toString();
            /* 
                cedula;fecha;nombre;email;direccion;genero(0=mujer, 1=hombre);codigo_municipio
                4068079;13/01/63;pepe1;pepe1@email.com;calle 1 - av 0;1;54001
                5148539;15/12/73;pepe2;pepe2@email.com;calle 1 - av 1;1;68001
                5274295;6/02/59;pepe3;pepe3@email.com;calle 1 - av 2;0;68001
                2371092;20/07/56;pepe4;pepe4@email.com;calle 1 - av 3;1;68001
                2762766;15/07/65;pepe5;pepe5@email.com;calle 1 - av 4;0;68001
                5999378;8/06/56;pepe6;pepe6@email.com;calle 1 - av 5;0;68001
                5718127;17/06/56;pepe7;pepe7@email.com;calle 1 - av 6;0;68001
                4115690;10/04/76;pepe8;pepe8@email.com;calle 1 - av 7;0;68001
                8020284;20/09/76;pepe9;pepe9@email.com;calle 1 - av 8;0;68001
                3734792;28/11/67;pepe10;pepe10@email.com;calle 1 - av 9;0;68001
                9228984;3/07/59;pepe11;pepe11@email.com;calle 1 - av 10;0;54001
                2434722;16/11/62;pepe12;pepe12@email.com;calle 1 - av 11;0;54001
                3667348;3/04/56;pepe13;pepe13@email.com;calle 1 - av 12;1;54001
                6863213;20/06/77;pepe14;pepe14@email.com;calle 1 - av 13;0;8001
                9777631;17/06/67;pepe15;pepe15@email.com;calle 1 - av 14;0;8001
                1853330;9/08/74;pepe16;pepe16@email.com;calle 1 - av 15;0;8001
                8723586;19/11/73;pepe17;pepe17@email.com;calle 1 - av 16;0;8001
                1518101;4/11/60;pepe18;pepe18@email.com;calle 1 - av 17;0;8001
                207037;2/08/66;pepe19;pepe19@email.com;calle 1 - av 18;0;8001
                8792585;31/10/61;pepe20;pepe20@email.com;calle 1 - av 19;0;54001
                4331513;17/08/66;pepe21;pepe21@email.com;calle 1 - av 20;1;54001
                810293;20/01/70;pepe22;pepe22@email.com;calle 1 - av 21;1;54001
                538455;27/05/69;pepe23;pepe23@email.com;calle 1 - av 22;0;8001
            */
            String datoPersona[]=registro.split(";");
            
            long personaCedula = Integer.parseInt(datoPersona[0]);
            LocalDate personafechaNac = LocalDate.parse(datoPersona[1]);
            String personaNombre = datoPersona[2];
            String personaCorreo = datoPersona[3];
            String personaDireccion = datoPersona[4];
            //suponinedo que los datos estan bien y solo hay 1 y 0
            boolean personaGenero = false;
            if(datoPersona[5]=="1"){
                personaGenero=true;
            }
            
            for(Region r:this.regiones)
            {
                for(int d=0;d<r.getDptos().getTamanio();d++)
                {
                    for(int m=0;m<r.getDptos().get(d).getMunicipios().getTamanio();m++)
                    {
                        if(r.getDptos().get(d).getMunicipios().get(m).getCodigo()==Integer.parseInt(datoPersona[6]))
                        {
                            Persona nuevaPersona = new Persona(personaCedula,personafechaNac,personaNombre,personaCorreo,personaDireccion,personaGenero);
                            r.getDptos().get(d).getMunicipios().get(m).getPersonas().enColar(nuevaPersona, nuevaPersona.getEdad());
                        }
                    }
                }
            }
            
        }
    }
    
    //Punto 4.a
    public int procesarSubsidios()
    {
        // :)
        return 0;
    }
    
    //Punto 4.b
    public int getCantidadSubsidioDepartamentos()
    {
        // :)
        return 0;
    }
    
    //Punto 4.c
    
    public ListaCD<Persona> getPersonasNoSubsidio()
    {
    
        // :)
        return null;
    }
    
    
    //Punto 4.d
    
    public Pila<Region> getSubisidiosRegion()
    {
        // :)
        return null;
    }
    
    
    //Punto 4.e
    public String getDatosPersona(long cedula)
    {
        // :)
        return "";
    }
    
    
    // Punto 4.f
    
    public int getCantidadSubsidioMunicipio(int codMunicipio)
    {
        // :)
        return 0;
    }
    
    //Punto 4.g
    public ListaCD<Persona> getDatosErroneos()
    {
        // :)
        return null;
    }
    
    
    
}
